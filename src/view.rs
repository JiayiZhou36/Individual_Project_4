use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

/// Request structure to deserialize incoming Lambda event payloads.
#[derive(Deserialize)]
struct Request {
    req_id: String,
    msg: String,            // Represents the trimmed message
    original_msg: String,   // Represents the original message
}

/// Response structure to serialize the response to be sent back.
#[derive(Serialize)]
struct Response {
    total_strings_trimmed: usize,
}

async fn function_handler(_event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Initialize AWS DynamoDB client
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    // Scan the DynamoDB table "stringtrim" to get all items
    let scan_result = client.scan().table_name("stringtrim").send().await?;

    // Count the total number of strings trimmed
    let total_strings_trimmed = match scan_result.items {
        Some(items) => {
            items.iter().filter(|item| {
                if let Some(trimmed_value) = item.get("trimmed_value") {
                    if let AttributeValue::S(trimmed_string) = trimmed_value {
                        !trimmed_string.is_empty() // Count only non-empty strings
                    } else {
                        false
                    }
                } else {
                    false
                }
            }).count()
        }
        None => 0, // If no items found, total count is zero
    };

    // Prepare the response
    let response = Response {
        total_strings_trimmed,
    };

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(function_handler)).await
}
