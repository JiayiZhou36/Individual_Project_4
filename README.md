# Individual Project 4
By Jiayi Zhou
[![pipeline status](https://gitlab.com/JiayiZhou36/Individual_Project_4/badges/main/pipeline.svg)](https://gitlab.com/JiayiZhou36/Individual_Project_4/-/commits/main)

## Video
[YouTube](https://youtu.be/Hd1t79suIKw)

## Purpose of Project
This project creates a Rust AWS Lambda function and Step Functions that trim the trailing space at the beginning and end of sentences, stores them in the database, and shows how many sentences are trimmed and stored in the database.

## Requirements
* Rust AWS Lambda function--three lambda functions that trim, store, and count strings
* Step Functions workflow coordinating Lambdas--connect three lambda functions
* Orchestrate data processing pipeline--process data, storage of processed data, and counting processed data in database

## Step Functions
![Screenshot_2024-04-14_at_12.46.48_AM](/uploads/b7c6b05c678d4b4c57d854f346d5e73d/Screenshot_2024-04-14_at_12.46.48_AM.png)

## Lambda Functions
![Screenshot_2024-04-15_at_10.34.45_PM](/uploads/52d3e91295fad4a98d4a6750ed799b48/Screenshot_2024-04-15_at_10.34.45_PM.png)
